# SLOManager #

SLOManager is the main component of SPECS (www.specs-project.eu) negotiation module. It represents the interface for negotiation of Security SLAs, exposing a simple REST API for interaction. 

### What is this repository for? ###

* SLOManager makes use of Supply Chain Manager, Planning  and SLAManager components in order to fulfill the negotiation activity of Security SLAs in the SPECS platform.
* Version: 0.1-SNAPSHOT

## Installation ##

### Install using precompiled binaries ###
The precompiled binaries are available under the SPECS Artifact Repository (http://ftp.specs-project.eu/public/artifacts/)

Prerequisites:

 * Oracle Java JDK 7;
 * MongoDB
 * Java Servlet/Web Container (recommended: Apache Tomcat 7)

Installation steps:

 * Download the web application archive (war) from the artifact repository (http://ftp.specs-project.eu/public/artifacts/sla-negotiation/slo-manager/slomanager-api-0.1-SNAPSHOT.war)
 * The application has to be deployed to the Application Server (Tomcat)
 * If Apache Tomcat 7.0.x is used, the war file needs to be copied into the “/webapps” folder inside the home directory (CATALINA_HOME) of Apache Tomcat 7.0.x.
 * MongoDB has to be up and running, listening on the default port of 27017.

### Install from source ###
Prerequisites:

 * Git client
 * Apache Maven:
 * Maven dependencies (not available in the maven repo, have to be downloaded and installed):
    - specs-data-model (https://bitbucket.org/specs-team/specs-utility-data-model.git)
    - specs-negotiation-supply-chain-manager (https://bitbucket.org/specs-team/specs-core-negotiation-supply_chain_manager)
 * Apache Tomcat
 * MongoDB
 * SPECS components:
    1. planning-api (https://bitbucket.org/specs-team/specs-enforcement-planning)
    2. service-manager-api (https://bitbucket.org/specs-team/specs-sla_platform-service_manager-services_api)
    3. sla-manager-api (https://bitbucket.org/specs-team/specs-sla_platform-sla_manager-sla-api)
    4. security-reasoner-api (not available for now, not required for now)

Downloading the source code:

 * ```git clone https://adispataru@bitbucket.org/specs-team/specs-core-negotiation-slomanager.git```

Database configuration

 * MongoDB has to be up and running at the moment the application starts up. It should accept traffic by default on port 27017 

How to run tests
 * change directory in the root of the project
 * run ```mvn test```

Deployment instructions:

Manually deployment to tomcat:

 * change directory in the root of the project
 * run ```mvn package```
 * in directory 'target' you will find slomanager-api.war. Deploy it to your Application Server (Tomcat) by copying it into the “/webapps” folder inside the home directory (CATALINA_HOME) of Apache Tomcat 7.0.x.

Maven tomcat deployer:

 * Create a user as script manager in tomcat-users.xml: ```<user name="deploy" password="s3cret" roles="manager-script" />```
 * change directory in the root of the project
 * run ```mvn tomcat7:deploy```
 * the application will be deployed on the context path */slomanager-api*

### REST API ###

* The REST API uses the following methods over resources:
    1. SLATemplates:
        - ```GET```      /sla-negotiation/sla-templates -> Retrieves the collection of SLATemplates 
        - ```POST```     /sla-negotiation/sla-templates -> Creates a new SLATemplate based on the request body.
        - ```DELETE```   /sla-negotiation/sla-templates -> Deletes all SLATemplates in the database. 


        - ```GET```      /sla-negotiation/sla-templates/{t-id} -> Retrieves the SLATemplate with id *t-id* (this will also create an SLA for the template inside SLAManager component)
        - ```PUT```      /sla-negotiation/sla-templates/{t-id} -> Updates SLATemplate with id *t-id* based on the request body.
        - ```DELETE```   /sla-negotiation/sla-templates/{t-id} -> Deletes SLATemplate with id *t-id* from the database. 

    2. SLAOffers:
        - ```POST```     /sla-negotiation/sla-templates/{t-id} -> Updates SLATemplate with id *t-id* based on the request body, and creates SLAOffers compliant with the requested mechanisms. This will return a collection of SLAOffers
        - ```GET```      /sla-negotiation/sla-templates/{t-id}/slaoffers -> Retrieves the SLAOffers constructed for the template with id *t-id* 
        - ```IMPORTANT!``` The offers have to be created first (by the POST method). If not, an empty list of SLAOffers will be returned by the *GET* method.

        - ```GET```      /sla-negotiation/sla-templates/{t-id}/slaoffers/{offer-id} -> Retrieves the SLASLAOffer that has id *offer-id*, related to the template with id *t-id* 

        - ```PUT```      /sla-negotiation/sla-templates/{t-id}/slaoffers/current -> Deploys the SLAOffer contained in the request body as current, triggering implementation of the SLA, related to the template with id *t-id* 
        - ```GET```      /sla-negotiation/sla-templates/{t-id}/slaoffers/current -> Retrieves the current employed SLA.

### Who do I talk to? ###

* Adrian Spataru: <florin.spataru92@e-uvt.ro>