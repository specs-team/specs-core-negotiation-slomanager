package eu.specs.project.negotiation.slomanager.util.clients;


import eu.specs.datamodel.agreement.offer.AgreementOffer;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Logger;

import static eu.specs.project.negotiation.slomanager.util.clients.ClientsConfig.requestFactory;

/**
 * Created by adispataru on 7/27/15.
 * Class to statically interface with SLAManager
 */
public class SLAClient {

    private static String url = ClientsConfig.getSLAMANAGERURL() + "/slas";
    private static String slas = "/slas/";
    private static RestTemplate template = new RestTemplate(requestFactory);

    public static void addConvertersToTemplate(){
        template.getMessageConverters().add(new StringHttpMessageConverter());
        template.getMessageConverters().add(new Jaxb2RootElementHttpMessageConverter());
        template.getMessageConverters().add(new FormHttpMessageConverter());
    }

    private SLAClient(){

    }

    /**
     * Creates SLA in SLAManager
     * @param sla SLA to be created
     * @return Created SLA id
     */
    public static String createSLA(AgreementOffer sla){

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_XML);

        HttpEntity<AgreementOffer> request = new HttpEntity<AgreementOffer>(
                sla, headers);
        String url = ClientsConfig.getSLAMANAGERURL() + "/slas";

        return template
                .postForObject(url, request, String.class);
    }

    /**
     * Updates SLA value
     * @param id id of the SLA in SLAManager
     * @param offer new SLA value
     */
    public static void updateTemplateWithSelectedSLA(String id, AgreementOffer offer){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_XML);

        HttpEntity<AgreementOffer> request = new HttpEntity<AgreementOffer>(
                offer, headers);
        String url = ClientsConfig.getSLAMANAGERURL() + slas + id;

        template.put(url, request);
    }

    /**
     * Deletes sla in SLAManager
     * @param id SLA id
     */
    public static void deleteSLA(String id){
        template.getMessageConverters().add(new StringHttpMessageConverter());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_XML);

        HttpEntity<String> request = new HttpEntity<String>(
                id, headers);
        String url = ClientsConfig.getSLAMANAGERURL() + slas + id;

        template.getMessageConverters().add(new FormHttpMessageConverter());
        template.getMessageConverters().add(new Jaxb2RootElementHttpMessageConverter());
        template.delete(url, request);

    }

    /**
     *
     * @return SLAManager URL
     */
    public static String getUrl() {
        return url;
    }

    /**
     *
     * @return template for interfacing with SLAManager via REST.
     */
    public static RestTemplate getTemplate(){
        return template;
    }


}
