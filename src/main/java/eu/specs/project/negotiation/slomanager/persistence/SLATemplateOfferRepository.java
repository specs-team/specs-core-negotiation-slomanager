package eu.specs.project.negotiation.slomanager.persistence;


import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Interface for maintaining SLATemplate to offer mapping
 */
public interface SLATemplateOfferRepository extends MongoRepository<SLATemplateOffer, String> {

    /**
     * Finds SLAOffer mapping for template id
     * @param templateId template id
     * @return SLAOffer mapping
     */
    SLATemplateOffer findByTemplateId(String templateId);

}
