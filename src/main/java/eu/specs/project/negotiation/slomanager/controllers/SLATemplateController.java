package eu.specs.project.negotiation.slomanager.controllers;


import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.project.negotiation.slomanager.persistence.SLATemplateOffer;
import eu.specs.project.negotiation.slomanager.persistence.SLATemplateOfferRepository;
import eu.specs.project.negotiation.slomanager.persistence.TemplateRepository;
import eu.specs.project.negotiation.slomanager.util.Collection;
import eu.specs.project.negotiation.slomanager.util.clients.SLAClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;

import java.net.URI;
import java.util.List;

/**
 * Created by adispataru on 7/2/15.
 * This class represents logic for the SLATemplate creation/deletion/retrieval/update, as well as the start
 * of a new negotiation process inside the SPECS Platform.
 */
@RestController
@RequestMapping("sla-negotiation/sla-templates")
public class SLATemplateController {
    @Autowired
    TemplateRepository templatesRepository;
    @Autowired
    SLATemplateOfferRepository slaTemplateOfferRepoistory;

    private static String baseUrl;


    /**
     * Creates SLATemplate
     * @param offer template value
     * @param request http request
     * @return 200 OK: URI of created resource </br> Conflict: Resource with id already exists
     */
    @RequestMapping(method = RequestMethod.POST, consumes = "text/xml")
    public ResponseEntity<URI> offer(@RequestBody AgreementOffer offer, HttpServletRequest request){
        baseUrl = String.format("%s://%s:%d%s/sla-negotiation", request.getScheme(),  request.getServerName(),
                request.getServerPort(), request.getContextPath());
        baseUrl += "/sla-templates/";
        if (templatesRepository.findByName(offer.getName()) != null) {
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(URI.create(baseUrl  + offer.getName()));

            return new ResponseEntity<URI>(URI.create(baseUrl  + offer.getName()), headers, HttpStatus.CONFLICT);
        }
        templatesRepository.save(offer);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create(baseUrl + offer.getName()));

        return new ResponseEntity<URI>(URI.create(baseUrl  + offer.getName()), headers, HttpStatus.CREATED);

    }

    /**
     * Creates SLATemplate in SLAManager
     * @param id SLATemplate id
     * @return 200OK: URI of created resource </br> 404 NOT FOUND: template not found
     */
    @RequestMapping(method = RequestMethod.POST, produces = "text/xml", value = "{id}")
    public ResponseEntity<AgreementOffer> createSLAFromSLATemplate(@PathVariable("id") String id, @QueryParam("username") String username){

        AgreementOffer a = templatesRepository.findByName(id);

        if(a == null){
            return new ResponseEntity<AgreementOffer>(null, new HttpHeaders(), HttpStatus.NOT_FOUND);
        }

        if(username != null){
        	a.getContext().setAgreementInitiator(username);
        }

        String slaid = SLAClient.createSLA(a);
        //Set new name for template
        a.setName(slaid);
        SLATemplateOffer mapping = new SLATemplateOffer();
        mapping.setCloudSLAId(slaid);
        mapping.setTemplateId(a.getName());
        slaTemplateOfferRepoistory.save(mapping);
        return new ResponseEntity<AgreementOffer>(a, HttpStatus.OK);
    }

    /**
     *
     * @param id SLATemplate id
     * @return SLATemplate if 200 OK </br> 404 Not Found if template not found
     */
    @RequestMapping(method = RequestMethod.GET, produces = "text/xml", value = "{id}")
    public ResponseEntity<AgreementOffer> getByName(@PathVariable("id") String id){
        AgreementOffer a = templatesRepository.findByName(id);

        if(a == null){
            return new ResponseEntity<AgreementOffer>(null, new HttpHeaders(), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<AgreementOffer>(a, HttpStatus.OK);
    }

    /**
     *
     * @param request http request
     * @return 200 OK Collection of URI of resources </br>
     */
    @RequestMapping(method = RequestMethod.GET, produces = "text/xml")
    public Collection<AgreementOffer> getAll(HttpServletRequest request){
        baseUrl = String.format("%s://%s:%d%s/sla-negotiation", request.getScheme(),  request.getServerName(),
                request.getServerPort(), request.getContextPath());
        List<AgreementOffer> offers = templatesRepository.findAll();
        Collection<AgreementOffer> result = new Collection<AgreementOffer>();
        result.resource = "sla-templates";
        result.fromList(offers, baseUrl);
        return result;
    }


    /**
     * Deletes all SLATemplates
     * @return 204 NoContent
     */
    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity<Object> reset(){
        templatesRepository.deleteAll();
        return new ResponseEntity<Object>(null, HttpStatus.NO_CONTENT);
    }

    /**
     * Deletes SLATemplate
     * @param id template id
     * @return 204 NoContent if template was deleted </br> 404 NOT FOUND if template not found
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "{id}")
    public ResponseEntity<Object> deleteTemplate(@PathVariable("id") String id){
        if(templatesRepository.findByName(id) == null){
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
        templatesRepository.delete(id);
        return new ResponseEntity<Object>(null, HttpStatus.NO_CONTENT);
    }
}
