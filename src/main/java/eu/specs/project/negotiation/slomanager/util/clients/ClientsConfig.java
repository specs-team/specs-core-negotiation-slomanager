package eu.specs.project.negotiation.slomanager.util.clients;

import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * Created by adispataru on 7/27/15.
 * Class for configuration of leveraged components
 */
public class ClientsConfig {

    private static String planningUrl = null;
    private static String servicesManagerUrl = null;
    private static String slaManagerUrl = null;
    private static String securityReasonerUrl = null;
    static final ClientHttpRequestFactory requestFactory = new
            HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create()
            .setMaxConnTotal(200)
            .setMaxConnPerRoute(50)
            .build());


    private ClientsConfig(){

    }

    public static boolean clientsNotSet(){
        return planningUrl == null || servicesManagerUrl == null ||
                slaManagerUrl == null || securityReasonerUrl == null;
    }

    /**
     *
     * @return Planning component url
     */
    public static String getPLANNINGURL() {
        return planningUrl;
    }

    /**
     * Sets Planning component url
     * @param planningUrl url
     */
    public static void setPLANNINGURL(String planningUrl) {
        ClientsConfig.planningUrl = planningUrl;
    }

    /**
     *
     * @return ServicesManager component url
     */
    public static String getSERVICESMANAGERURL() {
        return servicesManagerUrl;
    }

    /**
     * Sets ServicesManager component url
     * @param servicesManagerUrl url
     */
    public static void setSERVICESMANAGERURL(String servicesManagerUrl) {
        ClientsConfig.servicesManagerUrl = servicesManagerUrl;
    }

    /**
     *
     * @return SLAManager component url
     */
    public static String getSLAMANAGERURL() {
        return slaManagerUrl;
    }

    /**
     * Sets SLAManagercomponent url
     * @param slaManagerUrl url
     */
    public static void setSLAMANAGERURL(String slaManagerUrl) {

        ((HttpComponentsClientHttpRequestFactory) requestFactory).setConnectionRequestTimeout(5000);
        ClientsConfig.slaManagerUrl = slaManagerUrl;
    }

    /**
     *
     * @return SercurityReasoner component url
     */
    public static String getSECURITYREASONERURL() {
        return securityReasonerUrl;
    }

    /**
     * Sets SecurityReasoner component url
     * @param securityReasonerUrl url
     */
    public static void setSECURITYREASONERURL(String securityReasonerUrl) {
        ClientsConfig.securityReasonerUrl = securityReasonerUrl;
    }

    public static ClientsConfig getPrivateObject() {
        return new ClientsConfig();
    }
}
