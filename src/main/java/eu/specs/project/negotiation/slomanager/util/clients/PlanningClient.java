package eu.specs.project.negotiation.slomanager.util.clients;

import org.springframework.web.client.RestTemplate;

import static eu.specs.project.negotiation.slomanager.util.clients.ClientsConfig.requestFactory;

/**
 * Created by adispataru on 7/27/15.
 * Class for interfacing with Planning component when performing operations regarding Supply Chains
 */
public class PlanningClient {

    private static RestTemplate template = new RestTemplate(requestFactory);
    private static String url = ClientsConfig.getPLANNINGURL();

    private PlanningClient(){

    }

    /**
     *
     * @return Rest Template
     */
    public static RestTemplate getTemplate() {
        return template;
    }

    /**
     * Deletes supply chain
     * @param scId Supply chain id
     */
    public static void deleteSC(String scId){
        template.delete(url + "/supply-chains/" + scId);
    }

    /**
     *
     * @return planning component url
     */
    public static String getUrl() {
        return url;
    }
}
