package eu.specs.project.negotiation.slomanager.controllers;

import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.agreement.terms.ServiceDescriptionTerm;
import eu.specs.datamodel.agreement.terms.Term;
import eu.specs.datamodel.agreement.terms.Terms;
import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.datamodel.sla.sdt.ServiceDescriptionType;
import eu.specs.project.negotiation.slomanager.persistence.SLATemplateOffer;
import eu.specs.project.negotiation.slomanager.persistence.SLATemplateOfferRepository;
import eu.specs.project.negotiation.slomanager.persistence.TemplateRepository;
import eu.specs.project.negotiation.slomanager.util.Collection;
import eu.specs.project.negotiation.slomanager.util.TermExtractor;
import eu.specs.project.negotiation.slomanager.util.clients.PlanningClient;
import eu.specs.project.negotiation.slomanager.util.clients.SCMClient;
import eu.specs.project.negotiation.slomanager.util.clients.SLAClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by adispataru on 7/2/15.
 * This class is the controller for the sla-templates API path.
 */
@RestController
@RequestMapping("sla-negotiation/sla-templates/{id}")
public class SLAOfferController {
    @Autowired
    SLATemplateOfferRepository slaRepository;
    @Autowired
    TemplateRepository templateRepository;

    private static final Logger LOG = Logger.getLogger("SLAOfferController");
    private static final String URL_PATTERN = "%s://%s:%d%s/sla-negotiation/sla-templates/%s";

    /**
     * Creates SLAOffers for a given template
     * @param id - template id
     * @param offer offer template value
     * @param request http request
     * @return 201 Created: Collection of SLAOffer </br> 404 NOT FOUND if template not found
     */
    @RequestMapping(method = RequestMethod.POST, consumes = "text/xml", value = "slaoffers", produces = "text/xml")
    public ResponseEntity<Object> offer(@PathVariable("id") String id, @RequestBody AgreementOffer offer, HttpServletRequest request){
        String baseUrl = String.format(URL_PATTERN, request.getScheme(),
                request.getServerName(), request.getServerPort(), request.getContextPath(),  id);

        SLATemplateOffer offers = slaRepository.findByTemplateId(id);
        if (offers != null) {
            List<SupplyChain> scIds = SCMClient.constructSCM(offer);
            LOG.info("Retrieved " + scIds.size() + " supply chains.");
            int i = 1;
            for(SupplyChain sc : scIds){
                //*magic* to map sc attributes into sla
                AgreementOffer slaOffer = new AgreementOffer(offer);
                slaOffer.setName(offers.getTemplateId() + "_offer" + i);
                ServiceDescriptionTerm initialSdt;

                initialSdt = (ServiceDescriptionTerm) new TermExtractor().getTermsOfType(offer.getTerms().getAll().getAll(),
                        ServiceDescriptionTerm.class).get(0);
                createOfferFromSCBasedOnSDT(initialSdt, sc, slaOffer);


                String createdId = SLAClient.createSLA(slaOffer);

                LOG.info(String.format("Created SLA with id %s", createdId));
                LOG.info(String.format("For supply chain with id %s", sc.getId()));

                offers.getOffers().put(slaOffer.getName(), slaOffer);
                offers.getScMapping().put(slaOffer.getName(), sc.getId());
                offers.getSlaMapping().put(slaOffer.getName(), createdId);
                offers.getSlaRanks().put(slaOffer.getName(), 100 - i);
                i += 1;

            }

            Collection<AgreementOffer> result = new Collection<AgreementOffer>();
            result.resource = "slaoffers";
            java.util.Collection<AgreementOffer> off = offers.getOffers().values();

            //Sort AgreementOffers based on their rank.
            List<String> list = new ArrayList<String>();
            sortAgreementOffers(offers, off, list);

            for(String s : list) {
                result.getItemList().add(baseUrl + "/slaoffers/" + s);
            }
            result.total = (long) list.size();
            result.members = (long) list.size();
            result.setItemList(result.getItemList());
            slaRepository.save(offers);




            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(URI.create(baseUrl + offer.getName()));

            LOG.info("Returning " + result.getItemList().size() + " offers out of " + (i -1));
            return new ResponseEntity<Object>(result, headers, HttpStatus.CREATED);

        }else {
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(URI.create(baseUrl + id));

            LOG.info(id + " not found!");

            return new ResponseEntity<Object>(null, headers, HttpStatus.NOT_FOUND);
        }

    }

    private void createOfferFromSCBasedOnSDT(ServiceDescriptionTerm initialSdt, SupplyChain sc, AgreementOffer slaOffer) {

        ServiceDescriptionTerm sdt = new ServiceDescriptionTerm();
        sdt.setName(initialSdt.getName());
        sdt.setServiceName(initialSdt.getServiceName());


        Iterator<ServiceDescriptionType.ServiceResources.ResourcesProvider> it =
                initialSdt.getServiceDescription().getServiceResources().get(0).getResourcesProvider().iterator();
        ServiceDescriptionType.ServiceResources.ResourcesProvider rp =
                new ServiceDescriptionType.ServiceResources.ResourcesProvider();

        //select resources provider with matching provider id.
        while(it.hasNext()){
            ServiceDescriptionType.ServiceResources.ResourcesProvider resources = it.next();
            if(resources.getId().equals(sc.getCloudResource().getProviderId())){
                setResourcesProvider(sc, rp, resources);
            }
        }
        sdt.setServiceDescription(new ServiceDescriptionType());
        sdt.getServiceDescription().getServiceResources().add(new ServiceDescriptionType.ServiceResources());
        sdt.getServiceDescription().getServiceResources().get(0).getResourcesProvider().add(rp);

        sdt.getServiceDescription().setCapabilities(initialSdt.getServiceDescription().getCapabilities());
        sdt.getServiceDescription().setSecurityMetrics(initialSdt.getServiceDescription().getSecurityMetrics());

        List<Term> all = slaOffer.getTerms().getAll().getAll();
        List<Term> newTerms = new ArrayList<Term>();
        for(int x = 0; x < all.size(); x++){
            if(all.get(x) instanceof ServiceDescriptionTerm){
                newTerms.add(sdt);
            }else{
                newTerms.add(all.get(x));
            }
        }
        slaOffer.setTerms(new Terms());
        slaOffer.getTerms().setAll(new Terms.All());
        slaOffer.getTerms().getAll().setAll(newTerms);

    }

    private void setResourcesProvider(SupplyChain sc, ServiceDescriptionType.ServiceResources.ResourcesProvider rp, ServiceDescriptionType.ServiceResources.ResourcesProvider resources) {
        rp.setDescription(resources.getDescription());
        rp.setId(resources.getId());
        rp.setLabel(resources.getLabel());
        rp.setMaxAllowedVMs(resources.getMaxAllowedVMs());
        rp.setName(resources.getName());
        rp.setZone(resources.getZone());
        rp.getVM().clear();
        for(ServiceDescriptionType.ServiceResources.ResourcesProvider.VM vm : resources.getVM()){
            if(vm.getAppliance().equals(sc.getCloudResource().getAppliance()) &&
                    vm.getHardware().equals(sc.getCloudResource().getVmType()))
                rp.getVM().add(vm);

        }
    }

    /**
     * sorts offers based on their rank and puts their URI in a list
     * @param offers Offers mapped to a template
     * @param off list of Offers
     * @param list list in which offers are put
     */
    public static void sortAgreementOffers(SLATemplateOffer offers, java.util.Collection<AgreementOffer> off, List<String> list) {
        for (AgreementOffer o : off) {
            int rank = offers.getSlaRanks().get(o.getName());
            boolean found = false;
            for (int i2 = 0; i2 < list.size(); i2++) {
                if (rank > offers.getSlaRanks().get(list.get(i2))) {
                    list.add(i2, o.getName());
                    found = true;
                    break;
                }
            }
            if(!found)
                list.add(o.getName());
        }
    }

    /**
     * Signs or updates signed SLAOffer
     * @param id template id
     * @param offer offer body
     * @return 200 OK, 201 CREATED: Offer was saved; BAD REQUEST: template not found
     */
    @RequestMapping(method = RequestMethod.PUT, consumes = "text/xml", value = "slaoffers/current",
            produces = "application/xml")
    public ResponseEntity<AgreementOffer> submitOffer(@PathVariable("id") String id, @RequestBody AgreementOffer offer){

        SLATemplateOffer template =  slaRepository.findByTemplateId(id);
        if (template != null) {
            HttpStatus status = HttpStatus.OK;
            if (template.getSelectedOffer() == null)
                status = HttpStatus.CREATED;
            template.setSelectedOffer(offer.getName());
            slaRepository.save(template);
            String offerName = offer.getName();
            offer.setName(template.getCloudSLAId());
            template.getOffers().put(offerName, offer);

            //update SLAManager
            SLAClient.updateTemplateWithSelectedSLA(template.getCloudSLAId(), offer);
            //deleteSLA rejected supply chains
            Iterator<Map.Entry<String, String>> iter = template.getScMapping().entrySet().iterator();
            while (iter.hasNext()){
                Map.Entry<String, String> next = iter.next();
                if(next.getKey().equals(offerName))
                    continue;
                PlanningClient.deleteSC(next.getValue());
                iter.remove();
            }
            //deleteSLA rejected SLAs
            iter = template.getSlaMapping().entrySet().iterator();
            while (iter.hasNext()){
                Map.Entry<String, String> next = iter.next();
                Logger.getLogger("Deleting").info(String.format("%s vs %s", next.getKey(), offer.getName()));
                SLAClient.deleteSLA(next.getValue());
                iter.remove();

            }
            slaRepository.save(template);

            return new ResponseEntity<AgreementOffer>(offer, status);
        }else {

            return new ResponseEntity<AgreementOffer>(null,
                    null, HttpStatus.BAD_REQUEST);
        }

    }


    /**
     *
     * @param id template id
     * @param offerId offer id
     * @param request http request
     * @return SLAOffer associated
     */
    @RequestMapping(method = RequestMethod.GET, produces = "text/xml", value = "slaoffers/{offerId}")
    public ResponseEntity<Object> getByName(@PathVariable("id") String id, @PathVariable("offerId") String offerId,
                                            HttpServletRequest request){
        String baseUrl = String.format(URL_PATTERN, request.getScheme(),
                request.getServerName(), request.getServerPort(), request.getContextPath(), id);
        baseUrl = baseUrl + "/sla-templates/";
        if("current".equals(offerId)){
            SLATemplateOffer template =  slaRepository.findByTemplateId(id);
            if (template != null) {

                HttpHeaders headers = new HttpHeaders();
                headers.setLocation(URI.create(baseUrl + id + "/slaoffers"));

                return new ResponseEntity<Object>(template.getOffers().get(template.getSelectedOffer()), headers, HttpStatus.OK);

            }else {

                HttpHeaders headers = new HttpHeaders();
                headers.setLocation(URI.create(baseUrl + id + "/slaoffers"));

                return new ResponseEntity<Object>(baseUrl, headers, HttpStatus.NOT_FOUND);

            }
        }
        SLATemplateOffer a = slaRepository.findByTemplateId(id);
        if(a == null){
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create(baseUrl + id + "/slaoffers/" + offerId));
        if (a.getOffers().get(offerId) != null) {
            return new ResponseEntity<Object>(a.getOffers().get(offerId), headers, HttpStatus.OK);
        }
        return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);

    }

    /**
     *
     * @param id template id
     * @param offerId offer id
     * @return Rank of SLAOffer
     */
    @RequestMapping(method = RequestMethod.GET, value = "slaoffers/{offerId}/rank")
    public Integer getSLAOfferRank(@PathVariable("id") String id, @PathVariable("offerId") String offerId){
        SLATemplateOffer templateOffer = slaRepository.findByTemplateId(id);
        Logger.getAnonymousLogger().info(templateOffer.getSlaRanks().get(offerId).toString());
        return templateOffer.getSlaRanks().get(offerId);
    }


    /**
     *
     * @param id template id
     * @param request http request
     * @return SLAOffers associated to template with id
     */
    @RequestMapping(method = RequestMethod.GET, produces = "text/xml", value = "slaoffers")
    public ResponseEntity<Collection<String>> getAll(@PathVariable("id") String id, HttpServletRequest request){
        String baseUrl = String.format(URL_PATTERN, request.getScheme(),
                request.getServerName(), request.getServerPort(), request.getContextPath(), id);

        SLATemplateOffer template = slaRepository.findByTemplateId(id);
        if (template == null){
            return new ResponseEntity<Collection<String>>(new Collection<String>(), HttpStatus.BAD_REQUEST);
        }
        List<AgreementOffer> offers = Collections.list(Collections.enumeration(template.getOffers().values()));
        Collection<String> result = new Collection<String>();
        List<String> offerIds = new ArrayList<String>();
        for (AgreementOffer offer : offers){
            offerIds.add(offer.getName());
        }
        result.resource = "slaoffers";
        result.fromList(offerIds, baseUrl);
        return new ResponseEntity<Collection<String>>(result, HttpStatus.OK);
    }
}
