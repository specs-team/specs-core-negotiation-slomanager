package eu.specs.project.negotiation.slomanager.util;

import eu.specs.datamodel.agreement.offer.AgreementOffer;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adispataru on 7/2/15.
 * Class used for representing collections to xml
 */
@XmlRootElement
@XmlType(propOrder = {"itemList"})
public class Collection<T>{

    /**
     * resource type
     */
    @XmlAttribute
    public java.lang.String resource;
    /**
     * total Items
     */
    @XmlAttribute
    public Long total = 0L;
    /**
     * items
     */
    @XmlAttribute
    public Long items = 0L;
    /**
     * members
     */
    @XmlAttribute
    public Long members = 0L;

    private List<String> itemList;

    /**
     * Return item list
     * @return List of String
     */
    public List<String> getItemList() {
        if (itemList == null)
            itemList = new ArrayList<String>();
        return itemList;
    }

    /**
     * Sets item list
     * @param itemList list of items
     */
    public void setItemList(List<String> itemList) {
        this.itemList = itemList;
    }

    /**
     * Creates collection resource from list
     * @param list resource list
     * @param baseURL baseURL for resource
     */
    public void fromList(List<T> list, String baseURL){
        this.itemList = new ArrayList<String>();
        for (T element : list){
            if (element instanceof AgreementOffer){
                String id =  ((AgreementOffer) element).getName();
                itemList.add(baseURL + "/" + resource + "/" + id);
            }else if(element instanceof String){
                itemList.add(baseURL + "/" + resource + "/" + element);
            }
            else
                itemList.add(baseURL + "/" + resource);
        }
        this.items = (long) list.size();
        this.total = (long) list.size();
        this.members = (long) list.size();
    }
}
