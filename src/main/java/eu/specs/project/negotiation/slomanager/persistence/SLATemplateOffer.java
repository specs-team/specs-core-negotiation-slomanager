package eu.specs.project.negotiation.slomanager.persistence;

import eu.specs.datamodel.agreement.offer.AgreementOffer;
import org.springframework.data.annotation.Id;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by adispataru on 8/10/15.
 * Class for maintaining offers for based on template
 */
public class SLATemplateOffer{
    @Id
    private String templateId;

    private String cloudSLAId;

    private Map<String, AgreementOffer> offers = new HashMap<String, AgreementOffer>();
    private Map<String, String> slaMapping = new HashMap<String, String>();

    private String selectedOffer;
    private Map<String, String> scMapping = new HashMap<String, String>();
    private Map<String, Integer> slaRanks = new HashMap<String, Integer>();

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public Map<String, AgreementOffer> getOffers() {
        return offers;
    }

    public String getSelectedOffer() {
        return selectedOffer;
    }

    public void setSelectedOffer(String selectedOffer) {
        this.selectedOffer = selectedOffer;
    }

    public String getCloudSLAId() {
        return cloudSLAId;
    }

    public void setCloudSLAId(String cloudSLAId) {
        this.cloudSLAId = cloudSLAId;
    }

    public Map<String, String> getSlaMapping() {
        return slaMapping;
    }


    public Map<String, String> getScMapping() {
        return scMapping;
    }

    public Map<String, Integer> getSlaRanks() {
        return slaRanks;
    }
}
