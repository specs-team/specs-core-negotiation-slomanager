package eu.specs.project.negotiation.slomanager.util.clients;


import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.project.negotiation.scm.SupplyChainManager;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by adispataru on 7/27/15.
 * Class for interfacing with Supply Chain Manager component
 */
public class SCMClient {

    private static final Logger LOG = Logger.getLogger("Supply Chain Client");
    private SCMClient(){

    }

    /**
     * Creates supply chains for a SLATemplate
     * @param slaTemplate template to be used
     * @return List of SupplyChain
     */
    public static List<SupplyChain> constructSCM(AgreementOffer slaTemplate){

        List<SupplyChain> result = new ArrayList<SupplyChain>();
        SupplyChainManager scm = new SupplyChainManager(ClientsConfig.getSERVICESMANAGERURL(), ClientsConfig.getPLANNINGURL());
        try {
            result = scm.buildSupplyChains(slaTemplate);
            return result;
        } catch (Exception e) {
            LOG.throwing(SCMClient.class.toString(), "constructSCM", e);
        }

        return result;
    }




}
