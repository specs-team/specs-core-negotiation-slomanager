package eu.specs.project.negotiation.slomanager.util;

import eu.specs.datamodel.agreement.terms.Term;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by adispataru on 9/11/15.
 * Utility class to extract terms from an Agreement offer.
 */
public class TermExtractor {

    /**
     * Extracts terms of type @param{c} from an Agreement offer
     * @param termList the list of abstract terms
     * @param c desired Term class
     * @return ArrayList
     */
    public List<Term> getTermsOfType(List<Term> termList, Class<?> c){
        List<Term> result = new ArrayList<Term>();
        for(Term t : termList){
            if (t.getClass() == c)
                result.add(t);
        }
        return result;
    }
}
