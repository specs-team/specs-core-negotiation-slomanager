package eu.specs.project.negotiation.slomanager.persistence;


import eu.specs.datamodel.agreement.offer.AgreementOffer;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Repository for maintaining SLATemplate state
 */
public interface TemplateRepository extends MongoRepository<AgreementOffer, String> {
    /**
     * Finds Template by Name
     * @param name template name
     * @return Agreement offer or null (if object does not exists in DB)
     */
    AgreementOffer findByName(String name);

}
