package eu.specs.project.negotiation.slomanager;

import eu.specs.project.negotiation.slomanager.util.clients.ClientsConfig;
import eu.specs.project.negotiation.slomanager.util.clients.SLAClient;
import org.apache.logging.log4j.core.config.ConfigurationException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main Spring-boot: Application
 */
@SpringBootApplication
public class Application extends SpringBootServletInitializer {

    private static final Logger LOG = Logger.getLogger(Application.class.toString());
    private static final String propertiesFile = "/slomanager.properties";

    /**
     * Sets up application
     */
    public static void setUp() throws IOException{
        Properties properties = new Properties();

        properties.load(Application.class.getResourceAsStream(propertiesFile));

        ClientsConfig.setPLANNINGURL(properties.getProperty("planning-api.address"));
        ClientsConfig.setSERVICESMANAGERURL(properties.getProperty("service-manager-api.address"));
        ClientsConfig.setSLAMANAGERURL(properties.getProperty("sla-manager-api.address"));
        ClientsConfig.setSECURITYREASONERURL(properties.getProperty("security-reasoner-api.address"));
        SLAClient.addConvertersToTemplate();

        if (ClientsConfig.clientsNotSet()){
            LOG.severe("Something went wrong while configuring the application.");
        }
    }

    /**
     * starts application
     * @param args none needed
     */
    public static void main(String[] args) throws IOException{
        setUp();
        SpringApplication.run(Application.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        try {
            setUp();
        } catch (IOException e) {
            LOG.info("slomanager.properties NOT FOUND in resources folder!\nMessage: ");
            LOG.log(Level.SEVERE, "SLOManager Properties", e);
        }
        return application.sources(Application.class);
    }


}

