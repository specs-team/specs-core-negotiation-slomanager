import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.project.negotiation.slomanager.Application;
import eu.specs.project.negotiation.slomanager.controllers.SLAOfferController;
import eu.specs.project.negotiation.slomanager.persistence.SLATemplateOffer;
import eu.specs.project.negotiation.slomanager.util.Collection;
import eu.specs.project.negotiation.slomanager.util.clients.ClientsConfig;
import org.apache.logging.log4j.core.config.ConfigurationException;
import org.junit.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by adispataru on 9/1/15.
 */
public class SetupTest {

//    @Test
//    public void testSetUp() throws Exception{
//        Application.propertiesFile = "/slomanager.properties";
//        Application.setUp();
//        assertEquals("http://localhost:8080/planning-api", ClientsConfig.getPLANNINGURL());
//        assertEquals("http://localhost:8080/service-manager/cloud-sla", ClientsConfig.getSERVICESMANAGERURL());
//        assertEquals("http://localhost:8080/sla-manager/cloud-sla", ClientsConfig.getSLAMANAGERURL());
//        assertEquals("http://localhost:8080/tbd", ClientsConfig.getSECURITYREASONERURL());
//    }


//    @Test
//    public void testWrongSetup() throws Exception{
//        Application.propertiesFile = "/wrong-slomanager.properties";
//        Application.setUp();
//        assertTrue(ClientsConfig.clientsNotSet());
//    }

    @Test
    public void testClientsNotSet(){
        ClientsConfig.setPLANNINGURL("planning");
        assertTrue(ClientsConfig.clientsNotSet());
        ClientsConfig.setSERVICESMANAGERURL("service");
        assertTrue(ClientsConfig.clientsNotSet());
        ClientsConfig.setSLAMANAGERURL("slamanager");
        assertTrue(ClientsConfig.clientsNotSet());
        ClientsConfig.setSECURITYREASONERURL("sr");
        assertFalse(ClientsConfig.clientsNotSet());
        assertNotNull(ClientsConfig.getPrivateObject());
    }


    @Test
    public final void testCollection() {
        AgreementOffer offer = new AgreementOffer();
        offer.setName("template");
        Collection<AgreementOffer> col = new Collection<AgreementOffer>();
        List<AgreementOffer> list = new ArrayList<AgreementOffer>();
        list.add(offer);
        col.fromList(list, "none");
        List<Object> list2 = new ArrayList<Object>();

        list2.add(new Double(3));
        Collection<Object> col2 = new Collection<Object>();
        col2.fromList(list2, "none");


    }

    @Test
    public void testAgreementOfferSorting(){
        SLATemplateOffer slaTemplateOffer = new SLATemplateOffer();
        java.util.Collection<AgreementOffer> offers = new ArrayList<AgreementOffer>();
        Random r = new Random();
        for(int i = 0 ; i < 10; i++){
            AgreementOffer o = new AgreementOffer();
            o.setName("o"+i);
            slaTemplateOffer.getSlaRanks().put(o.getName(), r.nextInt(100));
            offers.add(o);
        }
        List<String> result = new ArrayList<String>();
        SLAOfferController.sortAgreementOffers(slaTemplateOffer, offers, result);
        assertTrue(result.size() > 0);
        for(int i = 0; i < result.size() - 1; i++){
            assertNotEquals(result.get(i), result.get(i+1));
            assertTrue(slaTemplateOffer.getSlaRanks().get(result.get(i))
                    >= slaTemplateOffer.getSlaRanks().get(result.get(i + 1)));
        }
    }
}
