package eu.specs.project.negotiation.test;

import eu.specs.datamodel.agreement.Context;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.agreement.slo.CustomServiceLevel;
import eu.specs.datamodel.agreement.slo.ServiceLevelObjective;
import eu.specs.datamodel.agreement.slo.ServiceProperties;
import eu.specs.datamodel.agreement.slo.Variable;
import eu.specs.datamodel.agreement.terms.GuaranteeTerm;
import eu.specs.datamodel.agreement.terms.ServiceDescriptionTerm;
import eu.specs.datamodel.agreement.terms.ServiceReference;
import eu.specs.datamodel.agreement.terms.Terms;
import eu.specs.datamodel.control_frameworks.AbstractSecurityControl;
import eu.specs.datamodel.control_frameworks.nist.NISTSecurityControl;
import eu.specs.datamodel.metrics.*;
import eu.specs.datamodel.sla.sdt.*;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by adispataru on 9/19/15.
 */
public class Util {

    private Util(){

    }

    public static AgreementOffer createAgreementOffer(String name){
        AgreementOffer a = new AgreementOffer();
        a.setName(name);
        //Context
        Context c = new Context();
        c.setAgreementInitiator("SPECS-CUSTOMER");
        c.setAgreementResponder("SPECS");
        c.setExpirationTime(new Date());
        c.setTemplateName("default");
        a.setContext(c);

        ServiceDescriptionTerm sdt = new ServiceDescriptionTerm();
        sdt.setName("Secure Web Server");
        sdt.setServiceName("SecureWebServer");
        //Service descr
        ServiceDescriptionType sd = new ServiceDescriptionType();
        CapabilityType cap = new CapabilityType();
        cap.setDescription("Capability of surviving to security incidents involving a web server, by implementing proper strategies aimed at preserving business continuity, achieved through redundancy and/or diversity.");

        cap.setId("WEBPOOL");
        cap.setName("Web Resilience");

        ControlFramework cf = new ControlFramework();
        cf.setFrameworkName("NIST Control framework 800-53 rev. 4");
        cf.setId("NIST_800_53_r4");

        NISTSecurityControl nistControl = new NISTSecurityControl();
        nistControl.setId("WEBPOOL_NIST_CP_2");
        nistControl.setImportanceWeight(WeightType.fromValue("MEDIUM"));
        nistControl.setName("CONTINGENCY PLAN - CP-2");
        nistControl.setControlFamily("SC");
        nistControl.setSecurityControl("29");
        nistControl.setControlEnhancement("0");
        nistControl.setControlDescription("The organization develops a contingency plan for the information system for achieving continuity of operations for mission/business functions. Contingency planning addresses both information system restoration and implementation of alternative mission/business processes when systems are compromised.");

        cf.setSecurityControl(new ArrayList<AbstractSecurityControl>());
        cf.getSecurityControl().add(nistControl);
        cap.setControlFramework(cf);
        sd.setCapabilities(new ServiceDescriptionType.Capabilities());
        sd.getCapabilities().getCapability().add(cap);

        AbstractMetricType securityMetricType = new AbstractMetricType();
        securityMetricType.setName("Level of Redundancy - ping");
        securityMetricType.setReferenceId("M1_redundancy");
        AbstractMetricType.AbstractMetricDefinition metricDefinition = new AbstractMetricType.AbstractMetricDefinition();
        UnitType unit = new UnitType();
        unit.setName("number of replicas");
        IntervalType interval = new IntervalType();
        interval.setIntervalItemsType("integer");
        interval.setIntervalItemStart("1");

        unit.setIntervalUnit(interval);
        metricDefinition.setUnit(unit);
        ScaleType scale = new ScaleType();
        scale.setQuantitative(QuantitativeValueType.fromValue("Ratio"));
        metricDefinition.setScale(scale);
        metricDefinition.setExpression("The number of active replicas is checked at each observation instant ti, where the ti are chosen \n" +
                "according to an \"ObservationInterval\" specified in ParameterDefinition and the check strategy is specified in RuleDefinition.");
        MetricRuleType metricRule = new MetricRuleType();
        metricRule.setNote("This rule defines how to check the number of active replicas: the check may be done according to different strategies:{ping,heartbit}");
        metricRule.setRuleDefinitionId("AMR_LoR_CheckStrategy");
        metricRule.setValue("ping");

        MetricType.MetricRules mRules = new MetricType.MetricRules();
        mRules.setMetricRule(metricRule);

        MetricType.MetricParameters mParams = new MetricType.MetricParameters();
        MetricParameterType mp = new MetricParameterType();
        mp.setNote("This parameter refers to the time, expressed in seconds, between two subsequent observations");
        mp.setParameterDefinitionId("AMP_LoR_ObservationInterval");
        mp.setValue("2");
        mParams.setMetricParameter(mp);

        MetricType metric = new MetricType();
        metric.getMetricParameters().add(mParams);
        metric.getMetricRules().add(mRules);
        securityMetricType.setAbstractMetricDefinition(metricDefinition);
        securityMetricType.getMetricParameters().add(mParams);
        securityMetricType.getMetricRules().add(mRules);
        sd.setSecurityMetrics(new ServiceDescriptionType.SecurityMetrics());
        sd.getSecurityMetrics().getSecurityMetric().add(securityMetricType);


        //Service Resources
        ServiceDescriptionType.ServiceResources serviceResources = new ServiceDescriptionType.ServiceResources();
        ServiceDescriptionType.ServiceResources.ResourcesProvider provider = new ServiceDescriptionType.ServiceResources.ResourcesProvider();
        provider.setId("aws-ec2");
        provider.setName("Amazon");
        provider.setZone("us-east-1");
        provider.setMaxAllowedVMs(20);
        ServiceDescriptionType.ServiceResources.ResourcesProvider.VM vm = new ServiceDescriptionType.ServiceResources.ResourcesProvider.VM();
        vm.setAppliance("us-east-1/ami-ff0e0696");
        vm.setHardware("t1.micro");
        vm.setDescr("open suse 13.1 on amazon EC2");
        provider.getVM().add(vm);
        serviceResources.getResourcesProvider().add(provider);
        sd.getServiceResources().add(serviceResources);




        sdt.setServiceDescription(sd);

        ServiceDescriptionTerm serviceProperties = new ServiceDescriptionTerm();
        serviceProperties.setName("Secure Web Server2");
        serviceProperties.setServiceName("SecureWebServer2");
        serviceProperties.setServiceDescription(new ServiceDescriptionType());



        //Service properties
        ServiceProperties serviceProperty = new ServiceProperties();
        serviceProperty.setName("//specs:capability[@id='WEBPOOL']");
        serviceProperty.setServiceName("SecureWebServer");
        Variable variable = new Variable();
        variable.setName("specs_webpool_M1");
        variable.setMetric("specs.main.java.negotiation.eu/metrics/M1_redundancy");
        variable.setLocation("//specs:securityControl[@nist:id='WEBPOOL_NIST_CP_2'] | //specs:securityControl[@nist:id='WEBPOOL_NIST_SC_5'] | //specs:securityControl[@nist:id='WEBPOOL_NIST_SC_36']");
        serviceProperty.setVariableSet(new ServiceProperties.VariableSet());
        serviceProperty.getVariableSet().getVariables().add(variable);
        Variable v2 = new Variable();
        v2.setName("specs_webpool_M2");
        v2.setLocation("//specs:securityControl[@nist:id='WEBPOOL_NIST_CP_2'] | //specs:securityControl[@nist:id='WEBPOOL_NIST_SC_5'] | //specs:securityControl[@nist:id='WEBPOOL_NIST_SC_29']");
        v2.setMetric("specs.main.java.negotiation.eu/metrics/M2_diversity");
        serviceProperty.getVariableSet().getVariables().add(v2);

        //Guarantee Term
        GuaranteeTerm guaranteeTerm = new GuaranteeTerm();
        guaranteeTerm.setName("//specs:capability[@id='WEBPOOL']");
        guaranteeTerm.setObligated("ServiceProvider");
        guaranteeTerm.setServiceLevelObjective(new ServiceLevelObjective());
        guaranteeTerm.getServiceLevelObjective().setCustomServiceLevel(new CustomServiceLevel());
        ObjectiveList objectiveList = new ObjectiveList();
        SLOType slo1 = new SLOType();
        slo1.setSLOID("webpool_slo1");
        slo1.setMetricREF("Level of Redundancy");
        slo1.setImportanceWeight(WeightType.MEDIUM);
        SLOexpressionType exp1 = new SLOexpressionType();
        exp1.setOneOpExpression(new SLOexpressionType.OneOpExpression());
        exp1.getOneOpExpression().setOperator(OneOpOperator.EQUAL);
        exp1.getOneOpExpression().setOperand("3");
        slo1.setSLOexpression(exp1);
        objectiveList.getSLO().add(slo1);

        SLOType slo2 = new SLOType();
        slo2.setSLOID("webpool_slo2");
        slo2.setMetricREF("Level of Diversity");
        slo2.setImportanceWeight(WeightType.MEDIUM);
        SLOexpressionType exp2 = new SLOexpressionType();
        exp2.setOneOpExpression(new SLOexpressionType.OneOpExpression());
        exp2.getOneOpExpression().setOperator(OneOpOperator.EQUAL);
        exp2.getOneOpExpression().setOperand("2");
        slo2.setSLOexpression(exp2);
        objectiveList.getSLO().add(slo2);

        guaranteeTerm.getServiceLevelObjective().getCustomServiceLevel().setObjectiveList(objectiveList);


        a.setTerms(new Terms());
        a.getTerms().setAll(new Terms.All());
        a.getTerms().getAll().getAll().add(sdt);
        a.getTerms().getAll().getAll().add(serviceProperties);
        a.getTerms().getAll().getAll().add(serviceProperty);
        a.getTerms().getAll().getAll().add(guaranteeTerm);
        ServiceReference sr = new ServiceReference();
        //testing service resources
        sr.setName("SecureWebServer_endpoint");
        sr.setServiceName("SecureWebServer");
        sr.setEndpoint("http://specs/application-endpoint");
        a.getTerms().getAll().getAll().add(sr);
        return a;
    }
}
