package eu.specs.project.negotiation.test;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.agreement.terms.ServiceDescriptionTerm;
import eu.specs.datamodel.sla.sdt.ServiceDescriptionType;
import eu.specs.project.negotiation.slomanager.Application;
import eu.specs.project.negotiation.slomanager.controllers.SLATemplateController;
import eu.specs.project.negotiation.slomanager.util.Collection;
import eu.specs.project.negotiation.slomanager.util.TermExtractor;
import eu.specs.project.negotiation.slomanager.util.clients.ClientsConfig;
import eu.specs.project.negotiation.slomanager.util.clients.PlanningClient;
import eu.specs.project.negotiation.slomanager.util.clients.SLAClient;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.test.web.client.response.MockRestResponseCreators;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.Properties;
import java.util.logging.Logger;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * Created by adispataru on 9/14/15.
 * This class tests the functionality of SLATemplateController.
 * The tests are with regard to the REST API at path /sla-negotiation/sla-templates
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration("server.port:0")
@ContextConfiguration(classes = {Application.class, ApplicationConfig.class})
@Profile("test")
public class SLATemplateControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    SLATemplateController controller;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private String path = "/sla-negotiation/sla-templates";

    private MockRestServiceServer slaManagerMockServer;
    private MockRestServiceServer planningMockServer;
    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8888);

//    @Autowired
//    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        Properties properties = new Properties();

        try {
            properties.load(Application.class.getResourceAsStream("/slomanager-test.properties"));

            ClientsConfig.setPLANNINGURL(properties.getProperty("planning-api.address"));
            ClientsConfig.setSERVICESMANAGERURL(properties.getProperty("service-manager-api.address"));
            ClientsConfig.setSLAMANAGERURL(properties.getProperty("sla-manager-api.address"));
            ClientsConfig.setSECURITYREASONERURL(properties.getProperty("security-reasoner-api.address"));
            if (ClientsConfig.getPLANNINGURL() == null || ClientsConfig.getSERVICESMANAGERURL() == null ||
                    ClientsConfig.getSLAMANAGERURL() == null || ClientsConfig.getSECURITYREASONERURL() == null){
                Logger.getAnonymousLogger().info("Something went wrong while configuring the application.");

            }

            SLAClient.addConvertersToTemplate();

        } catch (IOException e) {
            Logger.getAnonymousLogger().throwing(Application.class.toString(), "setUp", e);
        }
        slaManagerMockServer = MockRestServiceServer.createServer(SLAClient.getTemplate());
        planningMockServer = MockRestServiceServer.createServer(PlanningClient.getTemplate());
    }


    @Test
    public void retrieveTemplateNotFound() throws Exception {
        mockMvc.perform(get(path + "/id")
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isNotFound());

    }


    @Test
    public void deleteAllTemplates() throws Exception {
        mockMvc.perform(delete(path))
                .andExpect(status().isNoContent());

        mockMvc.perform(get(path).accept(MediaType.TEXT_XML))
                .andExpect(status().isOk())
                .andExpect(content().xml("<collection resource=\"sla-templates\" total=\"0\" items=\"0\" members=\"0\"/>"));
    }

    @Test
    public void negotiationFlow() throws Exception{

        String xml = readStream(this.getClass().getResourceAsStream("/SPECS_TEMPLATE_NIST.xml"));
        new ServiceManagerMock().mockSecurityMechanisms();
        new PlanningMock().mockPlanningApi();
        String templateName = "WebPool_SVA";

        slaManagerMockServer.expect(MockRestRequestMatchers.requestTo(SLAClient.getUrl()))
                .andExpect(MockRestRequestMatchers.method(HttpMethod.POST))
                .andRespond(MockRestResponseCreators.withSuccess("1", MediaType.TEXT_PLAIN));
        slaManagerMockServer.expect(MockRestRequestMatchers.requestTo(SLAClient.getUrl()))
                .andExpect(MockRestRequestMatchers.method(HttpMethod.POST))
                .andRespond(MockRestResponseCreators.withSuccess("2", MediaType.TEXT_PLAIN));
        slaManagerMockServer.expect(MockRestRequestMatchers.requestTo(SLAClient.getUrl()))
                .andExpect(MockRestRequestMatchers.method(HttpMethod.POST))
                .andRespond(MockRestResponseCreators.withSuccess("3", MediaType.TEXT_PLAIN));

        slaManagerMockServer.expect(MockRestRequestMatchers.requestTo("http://localhost:8888/sla-manager/cloud-sla/slas/1"))
                .andExpect(MockRestRequestMatchers.method(HttpMethod.PUT))
                .andExpect(MockRestRequestMatchers.header("Content-Type", "text/xml"))
                .andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK));

        slaManagerMockServer.expect(MockRestRequestMatchers.requestTo(SLAClient.getUrl() + "/2"))
                .andExpect(MockRestRequestMatchers.method(HttpMethod.DELETE))
                .andRespond(MockRestResponseCreators.withNoContent());

        slaManagerMockServer.expect(MockRestRequestMatchers.requestTo(SLAClient.getUrl() + "/3"))
                .andExpect(MockRestRequestMatchers.method(HttpMethod.DELETE))
                .andRespond(MockRestResponseCreators.withNoContent());

        planningMockServer.expect(MockRestRequestMatchers.requestTo(PlanningClient.getUrl() + "/supply-chains/" +
                "9721e990-585b-47f1-a6dd-a5048cf99e4b"))
                .andExpect(MockRestRequestMatchers.method(HttpMethod.DELETE))
                .andRespond(MockRestResponseCreators.withNoContent());

        slaManagerMockServer.expect(MockRestRequestMatchers.requestTo("http://localhost:8888/sla-manager/cloud-sla/slas/1"))
                .andExpect(MockRestRequestMatchers.method(HttpMethod.PUT))
                .andExpect(MockRestRequestMatchers.header("Content-Type", "text/xml"))
                .andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK));


        mockMvc.perform(post(path)
                .content(xml)
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isCreated());



        //get template
        MvcResult mvcResult = mockMvc.perform(get(path + "/" + templateName)
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isOk())
                .andReturn();

        String customSLA1 = mvcResult.getResponse().getContentAsString();


        mvcResult = mockMvc.perform(post(path + "/" + templateName)
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isOk())
                .andReturn();

        System.out.println("SLA Client address: " + SLAClient.getUrl());

        String customSLA = mvcResult.getResponse().getContentAsString();

        StringReader reader = new StringReader(customSLA);
        JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
        Unmarshaller u = jaxbContext.createUnmarshaller();
        AgreementOffer agreementOffer = (AgreementOffer) u.unmarshal(reader);

        assertEquals("1", agreementOffer.getName());
        ServiceDescriptionTerm term =  (ServiceDescriptionTerm) new TermExtractor().
                getTermsOfType(agreementOffer.getTerms().getAll().getAll(), ServiceDescriptionTerm.class).get(0);



        String offersPath = path +  "/" + agreementOffer.getName() + "/slaoffers";

        mockMvc.perform(get(offersPath + "/randomOfferName")
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isNotFound());

        mockMvc.perform(get(offersPath)
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isOk());

        mvcResult = mockMvc.perform(post(offersPath)
                .content(customSLA)
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isCreated())
                .andReturn();

        jaxbContext = JAXBContext.newInstance(Collection.class);

        Collection offers = (Collection) jaxbContext.createUnmarshaller().unmarshal(
                new StringReader(mvcResult.getResponse().getContentAsString()));

        assertEquals(2, offers.getItemList().size());

        mvcResult = mockMvc.perform(get(offers.getItemList().get(0).toString())
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isOk())
                .andReturn();

        jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
        AgreementOffer agreementOffer1 = (AgreementOffer) jaxbContext.createUnmarshaller().unmarshal(
                new StringReader(mvcResult.getResponse().getContentAsString()));

        ServiceDescriptionTerm sdt = (ServiceDescriptionTerm) new TermExtractor()
                .getTermsOfType(agreementOffer1.getTerms().getAll().getAll(),ServiceDescriptionTerm.class).get(0);

        assertNotNull(sdt);

        ServiceDescriptionType.ServiceResources.ResourcesProvider resources =
            sdt.getServiceDescription().getServiceResources().get(0).getResourcesProvider().get(0);

        assertEquals("ec2", resources.getId());
        assertEquals("eucalyptus", resources.getZone());

        assertEquals(1, resources.getVM().size());
        assertEquals("emi-65c3e382", resources.getVM().get(0).getAppliance());
        assertEquals("m1.large", resources.getVM().get(0).getHardware());



        String firstOfferPath = (String) offers.getItemList().get(0);
        String firstOfferRelativePath = firstOfferPath.split("sla-negotiation")[1];
        assertEquals("/sla-templates/1/slaoffers/1_offer1",
                firstOfferRelativePath);

        mvcResult = mockMvc.perform(get(offersPath)
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isOk())
                .andReturn();


        jaxbContext = JAXBContext.newInstance(Collection.class);

        offers = (Collection) jaxbContext.createUnmarshaller().unmarshal(
                new StringReader(mvcResult.getResponse().getContentAsString()));



        firstOfferPath = (String) offers.getItemList().get(0);
        String secondOfferPath = (String) offers.getItemList().get(1);
        firstOfferRelativePath = firstOfferPath.split("sla-negotiation")[1];
        assertEquals("/sla-templates/1/slaoffers/1_offer1",
                firstOfferRelativePath);


        mvcResult = mockMvc.perform(get(firstOfferPath)
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isOk())
                .andReturn();


        String offer1 = mvcResult.getResponse().getContentAsString();


        mvcResult = mockMvc.perform(get(firstOfferPath + "/rank")
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isOk())
                .andReturn();

        String rank = mvcResult.getResponse().getContentAsString();
        assertNotNull(rank);
        assertTrue(rank.length() > 0);

        mvcResult = mockMvc.perform(get(secondOfferPath + "/rank")
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isOk())
                .andReturn();

        String rank2 = mvcResult.getResponse().getContentAsString();
        assertNotNull(rank);
        assertTrue(rank.length() > 0);
        assertTrue(Integer.parseInt(rank) > Integer.parseInt(rank2));


        String currentOfferPath = offersPath + "/current";

        mockMvc.perform(put(currentOfferPath).content(offer1)
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isCreated());

        mockMvc.perform(put(currentOfferPath).content(offer1)
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isOk());

        mvcResult = mockMvc.perform(get(currentOfferPath).contentType(MediaType.TEXT_XML))
                .andExpect(status().isOk())
                .andReturn();

        String currentOfferString = mvcResult.getResponse().getContentAsString();

//        assertEquals(offer1, currentOfferString);

        jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
        AgreementOffer currentAgreementOffer = (AgreementOffer) jaxbContext.createUnmarshaller().unmarshal(
                new StringReader(currentOfferString));
        assertEquals(agreementOffer.getName(), currentAgreementOffer.getName());

        ServiceDescriptionTerm currentTerm =  (ServiceDescriptionTerm) new TermExtractor().
                getTermsOfType(currentAgreementOffer.getTerms().getAll().getAll(), ServiceDescriptionTerm.class).get(0);



        //check if capabilities are maintained
        assertNotNull(currentTerm.getServiceDescription().getCapabilities());
        assertEquals(currentTerm.getServiceDescription().getCapabilities().getCapability().size(),
                term.getServiceDescription().getCapabilities().getCapability().size());

        //check if security metrics are maintained
        assertNotNull(currentTerm.getServiceDescription().getSecurityMetrics());
        assertEquals(currentTerm.getServiceDescription().getSecurityMetrics().getSecurityMetric().size(),
                term.getServiceDescription().getSecurityMetrics().getSecurityMetric().size());





        slaManagerMockServer.verify();
        planningMockServer.verify();

        //servicesMockServer.verify();
    }

    @Test
    public void createAndDeleteTemplate() throws Exception{
        String xml = readStream(this.getClass().getResourceAsStream("/SPECS_TEMPLATE_NIST.xml"));
        JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
        Unmarshaller um = jaxbContext.createUnmarshaller();
        Marshaller marshaller = jaxbContext.createMarshaller();
        AgreementOffer template = (AgreementOffer) um.unmarshal(new StringReader(xml));
        template.setName("Test");
        StringWriter sw = new StringWriter();
        marshaller.marshal(template, sw);
        mockMvc.perform(post(path)
                .content(sw.toString())
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isCreated());

        mockMvc.perform(post(path + "/randomId")
                .content(sw.toString())
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isNotFound());

        mockMvc.perform(post(path)
                .content(sw.toString())
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isConflict());

        mockMvc.perform(delete(path + "/Test")
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isNoContent());

        mockMvc.perform(delete(path + "/Test")
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isNotFound());



        mockMvc.perform(delete(path)
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isNoContent());
        mockMvc.perform(delete(path + "/")
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isNoContent());
    }

    @Test
    public void offerErrors() throws Exception {

        String fakeTemplatePath = path + "/randomSLAName/slaoffers";


        String xml = readStream(this.getClass().getResourceAsStream("/SPECS_TEMPLATE_NIST.xml"));

        mockMvc.perform(get(fakeTemplatePath)
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isBadRequest());

        mockMvc.perform(get(fakeTemplatePath + "/someId").content(xml)
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isBadRequest());

        mockMvc.perform(post(fakeTemplatePath)
                .content(xml)
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isNotFound());

        String currentFakeOfferPath = fakeTemplatePath + "/current";

        mockMvc.perform(put(currentFakeOfferPath).content(xml)
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isBadRequest());

        mockMvc.perform(get(currentFakeOfferPath)
                .contentType(MediaType.TEXT_XML))
                .andExpect(status().isNotFound());
    }


    public static String readStream(InputStream is) {
        StringBuilder sb = new StringBuilder();
        try {
            Reader r = new InputStreamReader(is, "UTF-8");
            int c;
            while ((c = r.read()) != -1) {
                sb.append((char) c);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return sb.toString();
    }
}
