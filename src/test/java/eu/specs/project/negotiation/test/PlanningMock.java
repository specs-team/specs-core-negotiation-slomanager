package eu.specs.project.negotiation.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;
import eu.specs.datamodel.enforcement.SupplyChain;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class PlanningMock {

    private ObjectMapper objectMapper;

    public PlanningMock() throws Exception {
        objectMapper = new ObjectMapper();
    }

    public void mockPlanningApi() throws IOException {

        String supplyChainJson = Resources.toString(
                this.getClass().getResource("/supply-chain.json"), Charset.forName("UTF-8"));
//        SupplyChain supplyChain = objectMapper.readValue(supplyChainJson, SupplyChain.class);

        String supplyChainActivityJson = Resources.toString(
                this.getClass().getResource("/supply-chain-activity.json"), Charset.forName("UTF-8"));

        stubFor(get(urlEqualTo("http://localhost:8888/planning-api/supply-chains/1"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(supplyChainJson)));


        stubFor(get(urlPathMatching("/planning-api/sc-activities/.*/state"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"state\":\"COMPLETED\"}")));

        SupplyChain supplyChain1 = new SupplyChain();
        supplyChain1.setId(UUID.randomUUID().toString());
        supplyChain1.setScaId("53101e1d-64e6-4528-a80c-8fee91ae23a5");
        //supplyChain1.setSlaId(agreementOffer.getName());
        List<SupplyChain> supplyChains = Arrays.asList(supplyChain1);

        stubFor(get(urlPathMatching("/planning-api/sc-activities/.*/supply-chains"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(supplyChainJson)));

        stubFor(post(urlPathMatching("/planning-api/sc-activities"))
                .willReturn(aResponse()
                        .withStatus(201)
                        .withHeader("Content-Type", "application/json")
                        .withBody(supplyChainActivityJson)));

        stubFor(delete(urlPathMatching("/planning-api/supply-chains/.*"))
                .willReturn(aResponse()
                        .withStatus(204)
                        .withHeader("Content-Type", "application/json")
                        .withBody("")));


    }
}