package eu.specs.project.negotiation.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;
import eu.specs.datamodel.enforcement.SecurityMechanism;
import eu.specs.project.negotiation.scm.SupplyChainManager;
import eu.specs.project.negotiation.scm.utils.CollectionSchema;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class ServiceManagerMock {

    private ObjectMapper objectMapper;

    public ServiceManagerMock() throws Exception {
        objectMapper = new ObjectMapper();
    }

    public void mockSecurityMechanisms() throws IOException {

        String mechanismSvaJson = Resources.toString(
                this.getClass().getResource("/mechanism-sva.json"), Charset.forName("UTF-8"));
        SecurityMechanism mechanismSva = objectMapper.readValue(mechanismSvaJson, SecurityMechanism.class);
        CollectionSchema mechanismWebPool = new CollectionSchema();
        mechanismWebPool.setItemList(new ArrayList<String>());
        mechanismWebPool.getItemList().add("http://localhost:8888/service_manager/cloud-sla/security-mechanisms/sva");
        mechanismWebPool.setMembers(1);
        mechanismWebPool.setTotal(1);
        mechanismWebPool.setResource("security-mechanisms");

        stubFor(get(urlPathMatching("/service_manager/cloud-sla/security-mechanisms.*"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(objectMapper.writeValueAsString(mechanismWebPool))));

        stubFor(get(urlPathMatching("/service_manager/cloud-sla/security-mechanisms/sva"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(mechanismSvaJson)));

        String mechanismWebPoolJson = Resources.toString(
                this.getClass().getResource("/mechanism-webpool.json"), Charset.forName("UTF-8"));

        String mechanisJson = Resources.toString(
                this.getClass().getResource("/mechanisms.json"), Charset.forName("UTF-8"));

        stubFor(get(urlPathMatching("/service_manager/cloud-sla/security-mechanisms")).
                withQueryParam("metric_id", containing("level"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(mechanisJson)));

        stubFor(get(urlPathMatching("/service_manager/cloud-sla/security-mechanisms/webpool"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(objectMapper.writeValueAsString(mechanismWebPoolJson))));
    }
}