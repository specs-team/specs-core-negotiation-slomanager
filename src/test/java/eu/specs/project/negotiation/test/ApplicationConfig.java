package eu.specs.project.negotiation.test;

/**
 * Created by adispataru on 9/14/15.
 */

import com.github.fakemongo.Fongo;
import com.mongodb.Mongo;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories({"eu.specs.project.negotiation.slomanager.persistence"})
public class ApplicationConfig extends AbstractMongoConfiguration {
//    @Autowired
//    private Environment env;

    @Override
    protected String getDatabaseName() {
        //return env.getRequiredProperty("mongo.db.name");
        return "e-store";
    }

    @Override
    public Mongo mongo() throws Exception {
        return new Fongo(getDatabaseName()).getMongo();
    }
}